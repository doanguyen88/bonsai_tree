import React from 'react';
import './food-list.styles.css'
import { Food } from '../food/food.component'

export const FoodList = props => {
    console.log(props, 'FoodList props')
    return <div className='food-list'>
        {props.foods.map(food => (
            <Food key={food.id} food={food} />
        ))}
    </div>
}
