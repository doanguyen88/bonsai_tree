import React from 'react';
import './food.styles.css'

export const Food = props => {
    console.log(props, 'Food props')
    return <div className='food-container'>
        <img
            alt=''
            src={props.food.img} />
        <h2> {props.food.name} </h2>
        <p> {props.food.price} </p>
    </div>;
}