import React, { Component, Fragment } from 'react';
import './App.css';
import { FoodList } from './components/food-list/food-list.component';
import { SearchBox } from './components/search-box/search-box.component'

class App extends Component {
  constructor() {
    super();
    this.state = {
      foods: [
        {
          id: 1,
          img: 'https://images.foody.vn/res/g108/1070245/prof/s280x175/foody-upload-api-foody-mobile-da-210322113253.jpg',
          name: 'Bánh tráng trộn',
          price: 14000,
        },
        {
          id: 2,
          img: 'https://images.foody.vn/res/g108/1070643/prof/s280x175/foody-upload-api-foody-mobile-rectangle_large_type-210325141151.jpg',
          name: 'Chân gà xả ớt',
          price: 45000
        },
        {
          id: 3,
          img: 'https://images.foody.vn/res/g90/896051/prof/s280x175/file_restaurant_photo_iq1d_16109-9458f4e5-210118105107.jpeg',
          name: 'Bánh tiêu',
          price: 5000
        },
        {
          id: 4,
          img: 'https://images.foody.vn/res/g86/858482/prof/s280x175/foody-upload-api-foody-mobile-foody-ha-cao-kieu-ky-181212175634.jpg',
          name: 'Há cảo',
          price: 25000
        },
        {
          id: 5,
          img: 'https://images.foody.vn/res/g103/1027415/prof/s280x175/file_restaurant_photo_dl7y_16113-708479d6-210123155550.jpg',
          name: 'Bánh Plan',
          price: 7000
        },
        {
          id: 6,
          img: 'https://images.foody.vn/res/g2/19817/prof/s280x175/foody-mobile-hu-tieu-xao-mb-jpg-210-635829203269042708.jpg',
          name: 'Mì xào',
          price: 35000
        },
        {
          id: 7,
          img: 'https://images.foody.vn/res/g103/1025747/prof/s280x175/foody-upload-api-foody-mobile-220-200526141941.jpg',
          name: 'Phá lấu',
          price: 28000
        },
        {
          id: 8,
          img: 'https://images.foody.vn/res/g19/183094/prof/s280x175/foody-mobile-yfz3i0tr-jpg-557-636361552956446921.jpg',
          name: 'Cháo lòng bò',
          price: 32000
        },
        {
          id: 9,
          img: 'https://images.foody.vn/res/g95/949352/prof/s280x175/file_restaurant_photo_ntgd_16148-03df8a4a-210305000420.jpeg',
          name: 'Cơm gà',
          price: 42000
        }
      ],
      searchField: ''
    }
    this.onSearchChange = this.onSearchChange.bind(this)
  }

  onSearchChange = event => {
    this.setState({ searchField: event.target.value })
  }

  render() {
    const { foods, searchField } = this.state
    const filteredMonsters = foods.filter(food => food.name.toLowerCase().includes(searchField.toLowerCase()))
    return (
      <div className="App">
        <h1> Foods Of My Restaurant </h1>
        <SearchBox onSearchChange={this.onSearchChange} />
        <FoodList foods={filteredMonsters} />

        {/* {
          this.state.foods.map(food => <h2>{food.img}, {food.name}, {food.price} </h2>)
        } */}
      </div>
    );
  }
}
export default App;